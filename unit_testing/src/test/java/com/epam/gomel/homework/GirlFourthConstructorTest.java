package com.epam.gomel.homework;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class GirlFourthConstructorTest {

    private Girl testGirl = new Girl();

    @Test
    public void stepOne() {
        assertFalse(testGirl.isPretty());
    }

    @Test(dependsOnMethods = "stepOne")
    public void stepTwo() {
        assertTrue(testGirl.isSlimFriendGotAFewKilos());
    }

    @Test(dependsOnMethods = "stepTwo")
    public void stepThree() {
        assertNull(testGirl.getBoyFriend());
    }
}
