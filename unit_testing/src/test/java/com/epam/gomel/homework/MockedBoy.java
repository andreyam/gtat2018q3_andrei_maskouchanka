package com.epam.gomel.homework;

import java.time.Month;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MockedBoy {

    Boy instance() {

        Boy mockedBoy = mock(Boy.class);

        //doThrow(new RuntimeException()).when(mockedBoy).spendSomeMoney(10_000_000d);
        // stubbing
        when(mockedBoy.getWealth()).thenReturn(DataProviders.WEALTH_5);
        when(mockedBoy.getBirthdayMonth()).thenReturn(Month.AUGUST);
        when(mockedBoy.isRich()).thenReturn(true);
        when(mockedBoy.isPrettyGirlFriend()).thenReturn(true);
        when(mockedBoy.isSummerMonth()).thenReturn(true);
        return mockedBoy;
    }
}
