package com.epam.gomel.homework;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.time.Month;

public class BaseTest {

    @BeforeClass
    public Boy testBoy() {
        return new Boy(Month.JANUARY, DataProviders.WEALTH_5, null);
    }

    @AfterClass
    public void tearDown() {

    }
}
