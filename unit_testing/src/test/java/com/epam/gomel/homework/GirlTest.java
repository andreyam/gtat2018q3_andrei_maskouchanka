package com.epam.gomel.homework;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.time.Month;

import static com.epam.gomel.homework.GirlMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.not;
import static org.testng.Assert.*;
import static com.epam.gomel.homework.DataProviders.*;

@Test(groups = {"girl"})
public class GirlTest extends BaseTest {

    private Boy jackBoy = testBoy();
    private Boy dummyBoy = new MockedBoy().instance();
    private Girl maryGirl = new Girl(true, true, dummyBoy);

    @Test(groups = {"common"})
    public void testGirlSecondConstructor() {
        Girl testGirl = new Girl(true, true);
        assertNull(testGirl.getBoyFriend());
    }

    @Test(groups = {"common"}, priority = PRIORITY_LEVEL_2)
    public void testGetBoyFriend() {
        assertEquals(maryGirl.getBoyFriend(), jackBoy);
    }

    @Test(groups = {"function"},
            dependsOnMethods = "testIsBoyfriendRich", dataProvider = "money", dataProviderClass = DataProviders.class)
    public void testSpendBoyFriendMoney(double forSpend, double expected) {
        maryGirl.setBoyFriend(jackBoy);
        maryGirl.spendBoyFriendMoney(forSpend);
        assertEquals(jackBoy.getWealth(), expected);
    }

    @Test(groups = {"function"}, priority = PRIORITY_LEVEL_1)
    public void testIsBoyfriendRich() {
        maryGirl.setBoyFriend(dummyBoy);
        assertTrue(maryGirl.isBoyfriendRich());
    }

    @Test(groups = {"function"}, priority = PRIORITY_LEVEL_3)
    public void testIsBoyFriendWillBuyNewShoes() {
        maryGirl.setPretty(true);
        maryGirl.setBoyFriend(dummyBoy);
        assertTrue(maryGirl.isBoyFriendWillBuyNewShoes());
    }

    @Test(groups = {"function"}, dependsOnMethods = "testIsSlimFriendGotAFewKilos")
    public void testIsSlimFriendBecameFat() {
        maryGirl.setPretty(false);
        assertThat(maryGirl, both(not(pretty())).and(slimBoyFat()));
    }

    @Test(groups = {"common"})
    public void testSetAndGetPretty() {
        maryGirl.setPretty(true);
        assertTrue(maryGirl.isPretty());
    }

    @Test(groups = {"common"})
    @Parameters("wealth_10m")
    public void testGetAndSetBoyFriend(double wealth_10m) {
        Boy johnBoy = new Boy(Month.MARCH, wealth_10m, null);
        maryGirl.setBoyFriend(johnBoy);
        assertEquals(maryGirl.getBoyFriend(), johnBoy);
    }

    @Test(groups = {"common"})
    public void testIsSlimFriendGotAFewKilos() {
        assertTrue(maryGirl.isSlimFriendGotAFewKilos());
    }

}
