package com.epam.gomel.homework;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class GirlThirdConstructorTest {

    private Girl testGirl = new Girl(true);

    @Test
    public void stepOne() {
        assertFalse(testGirl.isSlimFriendGotAFewKilos());
    }

    @Test(dependsOnMethods = "stepOne")
    public void stepTwo() {
        assertNull(testGirl.getBoyFriend());
    }
}

