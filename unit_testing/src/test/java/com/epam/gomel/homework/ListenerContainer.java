package com.epam.gomel.homework;

import com.sun.jmx.snmp.Timestamp;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.util.Date;

public class ListenerContainer implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        String methodName = iInvokedMethod.getTestMethod().getMethodName();
        System.out.println(String.format("METHOD %1$s STARTED  - %2$s", methodName, getTimeStamp()));
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(String.format("METHOD %1$s FINISHED - %2$s >>> %3$s",
                iInvokedMethod.getTestMethod().getMethodName(), getTimeStamp(),
                iTestResult.getStatus() == 1 ? "SUCCESS" : "FAIL"));
        System.out.println(" ");
    }

    private String getTimeStamp() {

        Date date = new Date();
        long time = date.getTime();
        Timestamp timeStamp = new Timestamp(time);

        return timeStamp.getDate().toString();
    }
}
