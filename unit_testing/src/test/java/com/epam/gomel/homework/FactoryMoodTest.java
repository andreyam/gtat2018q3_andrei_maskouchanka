package com.epam.gomel.homework;

import org.testng.annotations.Factory;

public class FactoryMoodTest {

    @Factory(dataProvider = "boysWithMood", dataProviderClass = DataProviders.class)
    public Object[] generateTest(Boy boy) {
        System.out.println("Generate test for Boy.getMood() method");
        return new Object[]{new BoyMoodTest(boy)};
    }
}
