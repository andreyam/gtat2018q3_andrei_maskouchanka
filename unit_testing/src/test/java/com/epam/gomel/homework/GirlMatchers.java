package com.epam.gomel.homework;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import static org.hamcrest.Matchers.is;

class GirlMatchers {

    static Matcher<Girl> pretty() {
        return new BaseMatcher<Girl>() {
            @Override
            public boolean matches(Object object) {
                Girl girl = (Girl) object;
                return girl.isPretty();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("pretty should return true");
            }
        };
    }

    //SlimFriendGotAFewKilos
    static Matcher<Girl> slimBoyFat() {
        return new FeatureMatcher<Girl, Boolean>(is(true),
                "SlimFriendGotAFewKilos", "value") {
            @Override
            protected Boolean featureValueOf(Girl girl) {
                return girl.isSlimFriendGotAFewKilos();
            }
        };
    }
}
