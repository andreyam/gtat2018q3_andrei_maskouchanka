package com.epam.gomel.homework;

import org.testng.annotations.DataProvider;

import java.time.Month;

public class DataProviders {

    static final int PRIORITY_LEVEL_1 = 1;
    static final int PRIORITY_LEVEL_2 = 2;
    static final int PRIORITY_LEVEL_3 = 3;
    static final int PRIORITY_LEVEL_4 = 4;
    static final int PRIORITY_LEVEL_5 = 5;

    static final double WEALTH_05 = 5_000;
    static final double WEALTH_5 = 5_000_000;
    private static final double WEALTH_1 = 1_000_000;
    private static final double WEALTH_2 = 2_000_000;
    private static final double WEALTH_3 = 3_000_000;
    private static final double WEALTH_4 = 4_000_000;

    @DataProvider(name = "boys")
    public static Object[][] boysForTests() {
        Boy john = new Boy(Month.JUNE, WEALTH_1, null);
        Boy sam = new Boy(Month.JULY, WEALTH_2, null);
        Boy bill = new Boy(Month.AUGUST, WEALTH_3, null);
        return new Object[][] {
                {john},
                {sam},
                {bill}
        };
    }

    @DataProvider(name = "money")
    public static Object[][] amounts() {
        return new Object[][] {
                {WEALTH_1, WEALTH_4},
                {WEALTH_3, WEALTH_1}
        };
    }

    @DataProvider(name = "boysWithMood")
    public static Object[][] boysForMoodTests() {
        Boy john = new Boy(Month.MARCH, WEALTH_1, null);
        Girl girl = new Girl(false, false, john);
        Boy sam = new Boy(Month.MAY, WEALTH_1, girl);
        Boy bill = new Boy(Month.NOVEMBER, WEALTH_1, null);
        john.setGirlFriend(null);
        return new Object[][] {
                {john},
                {sam},
                {bill}
        };
    }
}
