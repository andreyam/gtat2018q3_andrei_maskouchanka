package com.epam.gomel.homework;

import org.testng.annotations.Test;
import java.time.Month;

import static org.testng.Assert.*;
import static com.epam.gomel.homework.DataProviders.*;

/**
 * Test getMood() method of Boy class
 */
public class BoyMoodTest {

    private static final double BOYS_WEALTH = 5_000_000;
    private Boy otherBoy;
    private Boy masterBoy = new Boy(Month.AUGUST, BOYS_WEALTH, null);
    private Girl girl = new Girl(true, false, masterBoy);

    BoyMoodTest(Boy boy) {
        this.otherBoy = boy;
    }

    @Test(priority = PRIORITY_LEVEL_1)
    public void testBranch1() {
        assertEquals(masterBoy.getMood(), Mood.EXCELLENT);
    }

    @Test(priority = PRIORITY_LEVEL_2)
    public void testBranch2() {
        Boy billyBoy = new Boy(Month.APRIL, BOYS_WEALTH, girl);
        assertEquals(billyBoy.getMood(), Mood.GOOD);
    }

    @Test(priority = PRIORITY_LEVEL_3)
    public void testBranch3() {
        girl.setPretty(false);
        assertEquals(masterBoy.getMood(), Mood.NEUTRAL);
    }

    @Test(priority = PRIORITY_LEVEL_4)
    public void testBranch4() {
        assertEquals(otherBoy.getMood(), Mood.BAD);
    }

    @Test(priority = PRIORITY_LEVEL_5)
    public void testBranch5() {
        Boy johnyBoy = new Boy(Month.NOVEMBER);
        assertEquals(johnyBoy.getMood(), Mood.HORRIBLE);
    }

}
