package com.epam.gomel.homework;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.time.Month;

import static com.epam.gomel.homework.GirlMatchers.pretty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.testng.Assert.*;

@Test(groups = {"boy"})
public class BoyTest extends BaseTest {

    private Girl girl = new Girl(true, false, new Boy(Month.JANUARY));
    private Boy boy1 = new Boy(Month.AUGUST, DataProviders.WEALTH_5, girl);
    private Boy boy2 = testBoy();

    @Test(groups = {"common"})
    public void testBoySecondConstructor() {
        assertNull(boy2.getGirlFriend());
    }

    @Test(groups = {"common"}, dependsOnMethods = "testGetWealth")
    public void testBoyThirdConstructorPart1() {
        Boy boy3 = new Boy(Month.OCTOBER);
        assertEquals(boy3.getWealth(), 0d);
    }

    @Test(groups = {"common"}, dependsOnMethods = "testBoyThirdConstructorPart1")
    public void testBoyThirdConstructorPart2() {
        Boy boy3 = new Boy(Month.OCTOBER);
        assertNull(boy3.getGirlFriend());
    }

    @Test(dependsOnMethods = "testBoySecondConstructor")
    public void testSetAndGetGirlFriend() {
        boy2.setGirlFriend(girl);
        assertEquals(boy2.getGirlFriend(), girl);
    }

    @Test(dependsOnMethods = "testGetWealth", dataProvider = "money", dataProviderClass = DataProviders.class)
    public void testSpendSomeMoney(double forSpend, double expected) {
        boy1.spendSomeMoney(forSpend);
        assertEquals(boy1.getWealth(), expected);
    }

    @Test(dependsOnMethods = "testSpendSomeMoney", expectedExceptions = RuntimeException.class)
    @Parameters("wealth")
    public void testSpendBigMoney(double wealth) {
        boy1.spendSomeMoney(wealth);
    }

    @Test(dataProvider = "boys", dataProviderClass = DataProviders.class)
    public void testIsSummerMonth(Boy boy) {
        assertTrue(boy.isSummerMonth(), "birthday Month should be Summer month");
    }

    @Test(dataProvider = "boys", dataProviderClass = DataProviders.class, dependsOnMethods = "testGetWealth")
    public void testIsRich(Boy boy) {
        assertTrue(boy.isRich(), "wealth should be over 1 000 000 ");
    }

    @Test(groups = {"common"})
    public void testIsPrettyGirlFriend() {
        assertThat(boy1.getGirlFriend(), allOf(notNullValue(), pretty()));
    }

    @Test(groups = {"common"})
    @Parameters("wealth")
    public void testGetWealth(double wealth) {
        assertEquals(boy1.getWealth(), wealth);
    }
}
