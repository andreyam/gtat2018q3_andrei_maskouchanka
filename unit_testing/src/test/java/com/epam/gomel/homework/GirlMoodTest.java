package com.epam.gomel.homework;

import org.testng.annotations.Test;
import java.time.Month;

import static com.epam.gomel.homework.DataProviders.*;
import static org.testng.Assert.assertEquals;

/**
 * Test getMood() method of Girl class
 */
public class GirlMoodTest {

    private static final double BOYS_WEALTH = 5_000_000;
    private static final double SMALL_WEALTH = 5_000;
    private Boy masterBoy = new Boy(Month.AUGUST, BOYS_WEALTH, null);
    private Girl girl = new Girl(true, false, masterBoy);

    @Test(priority = PRIORITY_LEVEL_1)
    public void testBranch1() {
        assertEquals(girl.getMood(), Mood.EXCELLENT);
    }

    @Test(priority = PRIORITY_LEVEL_2)
    public void testBranch2() {
        girl.setBoyFriend(new Boy(Month.APRIL, SMALL_WEALTH));
        assertEquals(girl.getMood(), Mood.GOOD);
    }

    @Test(priority = PRIORITY_LEVEL_3)
    public void testBranch3() {
        Girl neutralGirl = new Girl();
        assertEquals(neutralGirl.getMood(), Mood.NEUTRAL);
    }

    @Test(priority = PRIORITY_LEVEL_4)
    public void testBranch4() {
        Girl hateGirl = new Girl(false);
        assertEquals(hateGirl.getMood(), Mood.I_HATE_THEM_ALL);
    }
}
